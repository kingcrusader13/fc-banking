Rails.application.routes.draw do
  get 'welcome_index/index'

  resources :accounts
  resources :users
  
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'
  
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  
  get '/user/:username' => 'users#show'
  get '/accessdenied' => 'welcome_index#access_denied'
  
  get '/user/:username/edit' => 'users#edit'
  post '/user/:username/edit' => 'users#update'
  
  get '/accounts/:id/transactions/new' => 'transactions#new'
  post '/accounts/:id/transactions/new' => 'transactions#create'
  
  get '/accounts/:id/transactions/:transaction_id' => 'transactions#show'
  get '/accounts/:id/transactions' => 'transactions#index'
  
  get 'accounts/transactions/transfer' => 'transactions#new_transfer'
  post 'accounts/transactions/transfer' => 'transactions#transfer'
  
  get '/productivity' => 'time_waster#index'
  
  get '/snake' => 'time_waster#snake'
  get '/snake_score' => 'time_waster#snake_score'
  post '/time_waster' => 'time_waster#store_score'
  
  get '/pong' => 'time_waster#launch_pong', as: '/pong/play'
  get '/pong/servers' => 'time_waster#pong_server_browser'
  get '/pong/game/:id' => 'time_waster#pong'
  get '/pong/game/:id/begin' => 'time_waster#begin_pong_game'
  get '/pong/update/get/:id' => 'time_waster#pong_response'
  get '/pong/status/:id' => 'time_waster#pong_status'
  post '/pong/update/:id' => 'time_waster#pong_update_pos'
  
  root 'welcome_index#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
