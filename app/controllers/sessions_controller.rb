class SessionsController < ApplicationController
	def new
		@user = User.new
	end
	
	def create
		params[:username].downcase!
	
		@user = User.find_by username: params[:username]
		
		if @user && @user.authenticate(params[:password])
			session[:user_id] = @user.id
			redirect_to '/'
		else
			@user = User.new
			@user.errors.add(:username, " or password was incorrect")
			render 'new'
		end
	end
	
	def destroy
		session[:user_id] = nil
		redirect_to '/login'
	end
end
