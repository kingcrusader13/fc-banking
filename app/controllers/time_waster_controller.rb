require 'thread'

class TimeWasterController < ApplicationController
	before_action :authorize

	@@pong_games = {}
	
	def snake
	end
	
	def snake_score
		respond_to do |format|
		  format.js
		end
	end
	
	def store_score
		@game_score = current_user.game_scores.find_by_name(params[:name])
		
		if @game_score == nil
			current_user.game_scores.create(name: params[:name], high_score: params[:high_score].to_i)
		else
			@game_score.update_attributes(high_score: params[:high_score].to_i)
		end
		
		@game = Game.find_by_name(params[:name])
		
		if @game != nil
			if params[:high_score].to_i > @game.high_score
				@game.update_attributes(high_score: params[:high_score])
				@game.update_attributes(high_score_placer: current_user.username)
			end
		else
			Game.create(high_score: params[:high_score], name: params[:name],high_score_placer: current_user.username)
		end
	end
	
	def pong_server_browser
	end
	
	def pong_status
		params[:id] = params[:id].to_i
	
		respond_to do |format|
			format.json {
				render json: {
					game_began: pong_games(params[:id]).game_began,
					player_1_username: pong_games(params[:id]).player_1_username,
					player_2_username: pong_games(params[:id]).player_2_username
				}
			}
		end
	end
	
	def launch_pong
		game_id = @@pong_games.length
		
		pong_games(game_id)
		
		pong_games(game_id).player_1_username = current_user.username
		pong_games(game_id).player_2_username = nil;
		
		redirect_to '/pong/game/' + game_id.to_s
	end
	
	def pong
	end
	
	def begin_pong_game
		if @@pong_games[params[:id].to_i] != nil
			pong_games(params[:id].to_i).game_began = true;
		
			gameplay_thread = Thread.new do
				pong_gameplay(params[:id].to_i)
			end
			
			gameplay_thread.run()
		else
			redirect_to '/pong'
		end
	end
	
	def pong_update_pos
		hash = JSON.parse params[:json].to_json
	
		id = hash["id"].to_i
		
		if pong_games(id) != nil
			if hash["player_num"].to_i == 1
				pong_games(id).player_position_1 = hash["player_position"].to_i
				pong_games(id).last_player_response_1 = Time.now
			elsif hash["player_num"].to_i == 2
				pong_games(id).player_position_2 = hash["player_position"].to_i
				pong_games(id).last_player_response_2 = Time.now
			end
		end
		
		head 200, content_type: "text/html"
	end
	
	def pong_response
		params[:id] = params[:id].to_i

		winner_of_game = "Invalid"
		
		if pong_games(params[:id]).winner != ""
			winner_of_game = pong_games(params[:id]).winner
		end
		
		respond_to do |format|
			format.json {
				render json: {
					player_position_1: pong_games(params[:id]).player_position_1,
					player_position_2: pong_games(params[:id]).player_position_2,
					player_score_1: pong_games(params[:id]).player_score_1,
					player_score_2: pong_games(params[:id]).player_score_2,
					ball_position_x: pong_games(params[:id]).ball_position_x,
					ball_position_y: pong_games(params[:id]).ball_position_y,
					winner: winner_of_game
				}
			}
		end
	end
	
	def pong_games(id)
		@@pong_games ||= {}
		@@pong_games[id] ||= Pong.new()
	end
	
	def all_pong_games
		@@pong_games
	end
	private
	
	def pong_gameplay(id)
		id = id.to_i
	
		highest_score = 0;
		
		while highest_score < pong_games(id).goal do
			pong_mechanics(id)
		
			if pong_games(id).player_score_1 > highest_score
				highest_score = pong_games(id).player_score_1
			else
				highest_score = pong_games(id).player_score_2
			end
			
			sleep((Time.now + 0.033) - Time.now)
		end
		
		game_over(id)
	end
	
	def pong_mechanics(id)
		# Move the ball		
		pong_games(id).ball_position_x += pong_games(id).ball_velocity_x
		pong_games(id).ball_position_y += pong_games(id).ball_velocity_y
		
		# Check if the ball went over one side	
		if pong_games(id).ball_position_x <= 0 || pong_games(id).ball_position_x >= pong_games(id).canvas_width
			
			if pong_games(id).ball_position_x <= 0
				pong_games(id).player_score_2 += 1
			else
				pong_games(id).player_score_1 += 1
			end
			
			pong_games(id).ball_velocity_x = -pong_games(id).ball_velocity_x
			pong_games(id).ball_velocity_y = -pong_games(id).ball_velocity_y
			
			pong_games(id).ball_position_x = pong_games(id).canvas_width/2
			pong_games(id).ball_position_y = pong_games(id).canvas_height/2
		else
			adjusted_ball_pos_left = pong_games(id).ball_position_x - pong_games(id).ball_radius/2
			adjusted_ball_pos_right = pong_games(id).ball_position_x + pong_games(id).ball_radius/2
		
			# Check if there has been a "collision" and update velocity if so
			if adjusted_ball_pos_left <= pong_games(id).player_width
				adjusted_player_1_position_1 = pong_games(id).player_position_1 + pong_games(id).player_height
				adjusted_player_1_position_2 = pong_games(id).player_position_1 - pong_games(id).player_height
			
				if (pong_games(id).ball_position_y <= adjusted_player_1_position_1 && pong_games(id).ball_position_y >= adjusted_player_1_position_2)
					pong_games(id).ball_velocity_x = -pong_games(id).ball_velocity_x				
					pong_games(id).ball_velocity_y = (pong_games(id).player_position_1 - pong_games(id).previous_player_position_1)/30
				end
			elsif adjusted_ball_pos_right >= pong_games(id).canvas_width - pong_games(id).player_width
				adjusted_player_2_position_1 = pong_games(id).player_position_2 + pong_games(id).player_height
				adjusted_player_2_position_2 = pong_games(id).player_position_2 - pong_games(id).player_height
			
				if pong_games(id).ball_position_y <= adjusted_player_2_position_1 && pong_games(id).ball_position_y >= adjusted_player_2_position_2
					pong_games(id).ball_velocity_x = -pong_games(id).ball_velocity_x				
					pong_games(id).ball_velocity_y = (pong_games(id).player_position_2 - pong_games(id).previous_player_position_2)/30
				end
			end
		end
		
		# If the ball has hit the top or bottom of the canvas
		if pong_games(id).ball_position_y <= 0 || pong_games(id).ball_position_y >= pong_games(id).canvas_height
			pong_games(id).ball_velocity_y = -pong_games(id).ball_velocity_y
		end
	end
	
	def game_over(id)
		pong_games(id).winner = "";
		
		if pong_games(id).player_score_1 > pong_games(id).player_score_2
			pong_games(id).winner = pong_games(id).player_1_username
		else
			pong_games(id).winner = pong_games(id).player_2_username
		end
	end
end

class Pong
	attr_accessor :player_position_1
	attr_accessor :player_position_2
	
	attr_reader :player_height
	attr_reader :player_width
	
	attr_accessor :player_score_1
	attr_accessor :player_score_2
	
	attr_accessor :player_1_username
	attr_accessor :player_2_username
	
	attr_reader :canvas_height
	attr_reader :canvas_width
	
	attr_reader :ball_radius
	attr_accessor :ball_position_x
	attr_accessor :ball_position_y

	attr_accessor :last_player_response_1
	attr_accessor :last_player_response_2
	
	attr_accessor :winner
	
	attr_accessor :ball_velocity_x
	attr_accessor :ball_velocity_y
	
	attr_reader :goal
	
	attr_accessor :previous_player_position_1
	attr_accessor :previous_player_position_2
	
	attr_accessor :game_began
	
	def initialize
		reset()
	end

	def reset
		@game_began = false;
	
		@goal = 10;
	
		@ball_position_x = 0
		@ball_position_y = 0
		
		@ball_velocity_x = 8
		@ball_velocity_y = 4
		
		@ball_radius = 7
		
		@player_score_1 = 0
		@player_score_2 = 0
		
		@player_position_1 = 150
		@player_position_2 = 150
				
		@previous_player_position_1 = 0
		@previous_player_position_2 = 0
		
		@last_player_response_1 = Time.now
		@last_player_response_2 = Time.now
						
		@player_width = 10
		@player_height = 40
		
		@canvas_height = 300
		@canvas_width = 500
		
		@winner = ""
	end
end