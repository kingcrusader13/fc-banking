class UsersController < ApplicationController
	
	# Pages for reading data
	
	def index
		unless current_user && current_user.username == "kingcrusader13"
			redirect_to root_url
		end
		@user = User.all
	end
	
	def show
		if params[:username]
			@user = User.find_by_username(params[:username])
			
			unless current_user
				return redirect_to '/login'
			end
				
			unless @user != nil && current_user != nil && current_user.username == @user.username
				redirect_to '/accessdenied'
			end
		end
	end
	
	# Forms for Put and Post
	
	def new
		@user = User.new
	end

	def edit
		@user = User.find_by_username(params[:username])
		
		unless @user == current_user
			redirect_to '/accessdenied'
		end
	end
	
	# Put, Post, and Destroy methods
	
	def create
		params[:user][:username].downcase!
	
		@user = User.new(user_params)
		
		if @user.save
			redirect_to '/login'
		else
			render 'new'
		end
	end
	
	def update
		@user = User.find_by_username(params[:username])
		
		if @user.authenticate(params[:current_password])
			
			params[:state] = params[:state][:state]
		
			if @user.update_attributes(update_user_params)
				if params[:new_password] != nil && params[:new_password] != ""
					if params[:new_password] == params[:password_confirmation]
						@user.password = params[:new_password]
						@user.save
					else
						@user.errors.add(:new_password, " did not match confirmation!")
						render 'edit'
					end
				end
				redirect_to '/user/' + @user.username
			else
				render 'edit'
			end
		else
			render 'edit'
		end
	end
	
	def destroy
		@user = User.find(params[:id])
		
		if current_user && current_user == @user
			@user.destroy
		end
		
		redirect_to '/logout'
	end
	
	private

	# Params used for user creation
	def user_params
		params.require(:user).permit(:username, :password, :password_confirmation, :email, :city, :state, :zip_code, :address)
	end
	
	# Params used for updating users
	def update_user_params
		params.permit(:email, :city, :state, :zip_code, :address)
	end
end
