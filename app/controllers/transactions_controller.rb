class TransactionsController < ApplicationController
	def index
		@account = Account.find(params[:id])
		@transaction = @account.transactions
	end
	
	def show
		@transaction = Transaction.find(params[:transaction_id])
	end
	
	def new
		@transaction = Transaction.new
	end
	
	def create
	
		amount = params[:transaction_amount].to_f
	
		if params[:target][:target] != nil
			params[:target] = params[:target][:target]
		end
	
		unless params[:target] == "Deposit" || params[:target] == "Transfer Deposit"
			amount *= -1
		end
	
		@account = Account.find(params[:id])
		
		if @account.balance + amount >= 0
		
			target_operator = "";
			
			if amount > 0
				target_operator = "+"
			else
				target_operator = "-"
			end
		
			@transaction = @account.transactions.create(transaction_time: Time.now.asctime,transaction_amount: params[:transaction_amount], target: params[:target], operator: target_operator)
			
			new_balance = @account.balance + amount	
			@account.update_attributes(balance: new_balance)
			
			if @account.save
				redirect_to '/accounts/' + @account.id.to_s + '/transactions'
			else
				render 'new'
			end	
		else
			@transaction = Transaction.new
		
			@transaction.errors.add(:transaction_amount, " is invalid given current balance")
			render 'new'
		end
	end
	
	def new_transfer
		@transaction = Transaction.new
	end
	
	def transfer
		@transaction = Transaction.new
		
		@target = current_user.accounts.find_by_account_name(params[:target][:target])
		@source = current_user.accounts.find_by_account_name(params[:source][:source])
		
		amount = params[:amount].to_f
		
		if @source.balance >= amount
			new_balance = @source.balance - amount
			@source.update_attributes(balance: new_balance)
			@transaction = @source.transactions.create(transaction_time: Time.now.asctime,transaction_amount: amount, target: 'Transfer', operator: "-")
			
			new_balance = @target.balance + amount
			@target.update_attributes(balance: new_balance)
			@transaction = @target.transactions.create(transaction_time: Time.now.asctime,transaction_amount: amount, target: 'Transfer', operator: "+")
			
			redirect_to accounts_path
		else
			redirect_to '/accounts/transactions/transfer'
		end
	end
	
	private
	
	def creation_params
		params..permit(:transaction_amount, :target)
	end
	
	def transfer_params
		params.permit(:amount, :target, :source)
	end
end
