class AccountsController < ApplicationController
	before_action :authorize

	def index
	end
	
	def show
		@account = Account.find(params[:id])
		
		if @account.warning_threshold == nil
			@account.update(warning_threshold: 0)
		end
		
		unless current_user.id == @account.user_id
			redirect_to '/accessdenied'
		end
	end

	def edit
		@account = Account.find(params[:id])
		
		unless @current_user.id == @account.user_id
			redirect_to '/accessdenied'
		end
	end
	
	def new
		@account = Account.new
	end
	
	def create
		@account = current_user.accounts.create(balance: params[:account][:balance], account_type: params[:account][:account_type], account_name: params[:account][:account_name], warning_threshold: params[:account][:warning_threshold])
		
		if @account.save
			if @account.account_type == "Savings"
				@account.update(minimum_balance: 200)
				@account.update(interest_rate: 6)
				@account.update(transaction_counter: 3)
			else
				@account.update(minimum_balance: 0)
				@account.update(interest_rate: 0)
				@account.update(transaction_counter: 0)
			end
			
			@account.transactions.create(transaction_time: Time.now.asctime,transaction_amount: params[:account][:balance], target: 'Initial Deposit', operator: "+")
			
			
			redirect_to '/accounts'
		else
			render '/accounts/new'
		end
	end
	
	def update
		@account = Account.find(params[:id])
		
		if @account.update_attributes(account_update_params)
			redirect_to accounts_path
		else
			render 'edit'
		end
	end
	
	
	def destroy
		@account = Account.find(params[:id])
		
		if @account && current_user && @account.user_id == current_user.id
			@account.destroy
		end
		
		
		redirect_to '/accounts'
	end
	
	private
	
	def account_initial_params
		params.require(:account).permit(:account_type, :balance, :account_name, :warning_threshold)
	end
	
	def account_update_params
		params.require(:account).permit(:account_name, :warning_threshold)
	end
end
