class Transaction < ApplicationRecord
	belongs_to :account
	
	TRANSACTION_TYPES = ['Deposit', 'Withdrawal']
	
	validates :transaction_amount, presence: true
	validates :target, presence: true
end
