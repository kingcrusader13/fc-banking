class Account < ApplicationRecord
	belongs_to :user
	
	has_many :transactions, :dependent => :destroy
	
	POSSIBLE_ACCOUNT_TYPES = ['Checking', 'Savings']
	
	validates :balance, presence: true, numericality: { greater_than_or_equal_to: 0 }
	validates :account_name, presence: true, length: { maximum: 50 }
	
	attr_accessor :you
	attr_accessor :amount
	attr_accessor :account_selection
end
