class User < ApplicationRecord
	has_many :accounts, :dependent => :destroy
	has_many :game_scores, :dependent => :destroy

	STATES = ['AL', 'AK', 'AZ', 'AR', 'CA', 
	'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI', 
	'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 
	'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 
	'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 
	'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 
	'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 
	'WA', 'WV', 'WI', 'WY', 'Other' ]
	
	has_secure_password
	validates_presence_of :password, :on => :create
  
	validates :username, presence: true, length: { minimum: 5}, uniqueness: true
	validates :email, presence: true, uniqueness: true
	validates :address, presence: true
	validates :state, presence: true, length: {maximum: 2}
	validates :zip_code, presence: true, length: {minimum: 5, maximum: 5}
	validates :city, presence: true
	
	validate :password_complexity

	
	private

	def password_complexity
		return if password.nil?

		if password.size < 8
			errors.add :password, "Must be at least 8 characters long."
			return
		end
		
		passed = false
		
		if password.match(/[A-Z]/) && password.match(/[a-z]/) && password.match(/\d/)
			passed = true
		end
		
		if !passed
			errors.add(:password, " does not meet security requirements.")
		end
		
		return passed
	end
	
	def replace_password
		if self.authenticate(self.current_password)			
			if self.new_password != nil
				self.password = self.new_password
			end				
		else	
			errors.add(:current_password, " is incorrect.")
		end
	end
	
	def try_login(password_attempt)
		if self.authenticate(password_attempt)
			return true
		else
			return false
		end
	end
end