# Getting Started

To get this project running, open a terminal at the root of the project.

Then, run the following commands:

```shell
bundle install
```

```shell
ruby bin/rails server
```

# Running with Docker

To run this project using Docker, open a terminal at the root of the project.

Then, run the following command:

```shell
docker-compose up --build
```

You can then use Ctrl + C to close/shutdown the server.