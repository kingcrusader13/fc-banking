class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.string :transaction_date
      t.string :transaction_time

      t.timestamps
    end
  end
end
