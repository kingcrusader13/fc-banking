class AddBalanceAndTypeToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :balance, :double
    add_column :accounts, :type, :string
  end
end
