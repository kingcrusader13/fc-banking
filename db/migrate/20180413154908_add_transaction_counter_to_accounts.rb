class AddTransactionCounterToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :transaction_counter, :integer
  end
end
