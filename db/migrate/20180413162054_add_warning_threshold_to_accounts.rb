class AddWarningThresholdToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :warning_threshold, :double
  end
end
