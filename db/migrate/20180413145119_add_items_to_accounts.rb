class AddItemsToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :minimum_balance, :double
    add_column :accounts, :interest_rate, :double
    add_column :accounts, :warning_threshold, :string
  end
end
