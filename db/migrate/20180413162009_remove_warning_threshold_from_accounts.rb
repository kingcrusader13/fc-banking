class RemoveWarningThresholdFromAccounts < ActiveRecord::Migration[5.1]
  def change
    remove_column :accounts, :warning_threshold, :string
  end
end
