class AddConfirmPasswordToAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :confirm_password, :string
  end
end
