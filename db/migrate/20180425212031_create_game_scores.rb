class CreateGameScores < ActiveRecord::Migration[5.1]
  def change
    create_table :game_scores do |t|
      t.integer :high_score
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
