class RemoveUsernameFromGames < ActiveRecord::Migration[5.1]
  def change
    remove_column :games, :username, :string
  end
end
