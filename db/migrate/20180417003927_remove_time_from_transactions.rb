class RemoveTimeFromTransactions < ActiveRecord::Migration[5.1]
  def change
    remove_column :transactions, :transaction_date, :string
    remove_column :transactions, :transaction_time, :string
  end
end
