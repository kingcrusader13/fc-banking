class AddOperatorToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :operator, :string
  end
end
