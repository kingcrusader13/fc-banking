class RemoveAccountIdsFromAccounts < ActiveRecord::Migration[5.1]
  def change
    remove_column :accounts, :account_id, :integer
  end
end
