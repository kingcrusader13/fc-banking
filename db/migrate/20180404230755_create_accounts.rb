class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :username
      t.string :password
      t.integer :account_id

      t.timestamps
    end
  end
end
