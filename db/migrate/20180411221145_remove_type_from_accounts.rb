class RemoveTypeFromAccounts < ActiveRecord::Migration[5.1]
  def change
    remove_column :accounts, :type, :string
  end
end
