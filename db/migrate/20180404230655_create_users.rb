class CreateUsers < ActiveRecord::Migration[5.1]
  def change
	create_table "users", force: :cascade do |t|
		t.string "email"
		t.string "password_digest"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
		t.string "username"
		t.string "address"
		t.string "state"
		t.integer "zip_code"
		t.string "city"
		t.integer "access_level"
    end
  end
end
