class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.integer :high_score
      t.string :name

      t.timestamps
    end
  end
end
