class RemoveHighScorePlaceFromGames < ActiveRecord::Migration[5.1]
  def change
    remove_column :games, :high_score_place, :string
  end
end
