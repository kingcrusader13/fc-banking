class AddUserToGames < ActiveRecord::Migration[5.1]
  def change
    add_column :games, :high_score_place, :string
  end
end
