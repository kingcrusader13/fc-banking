class AddTimeToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :transaction_time, :string
  end
end
