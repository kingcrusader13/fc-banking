FROM ruby:2.5.1
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

COPY . ./app
WORKDIR /app

RUN bundle install

EXPOSE 80

CMD ["bundle", "exec", "rails", "server", "-p", "80"]